// SimpleMechanicalDevice.cpp : Defines the entry point for the console application.
//


#include "stdafx.h"

#include <math.h>
#include <iostream>
#include "supportClass.h"
#include "Mesh.h"

using namespace std;

#define PI			3.1415926

#define DEG2RAD (PI/180.0f)

int		screenWidth = 600;
int		screenHeight = 600;

bool	bWireFrame = false;

float camera_angle;
float camera_height;
float camera_dis;
float camera_X, camera_Y, camera_Z;
float lookAt_X, lookAt_Y, lookAt_Z;
bool b4View = false;

float	width = 0.2;
float	thickness = 0.06;
float	height1 = 1.5;
float	height2 = 3;
float	length = 0.6;  // length of CrankOval

float	baseRadius = 0.8;
float	baseHeight = 0.25;
float	baseRotateStep = 5;

float	columnSizeX = width;
float	columnSizeY = 4;
float	columnSizeZ = width;

float	rack1CuboidSizeX = width;
float	rack1CuboidSizeY = width;
float	rack1CuboidSizeZ = 3 * width;


float	rack1HoleCuboidWidth = thickness;
float	rack1HoleCuboidSizeY = width + 2 * thickness;
float	rack1HoleCuboidSizeZ = width + 2 * thickness;
float	rack1HoleCuboidSizeX = width + 2 * thickness;


float	slider1CuboidSizeX = width;
float	slider1CuboidSizeY = columnSizeY * 0.6; //
float	slider1CuboidSizeZ = width;

float	slider1HoleOvalHeight = columnSizeX;
float	slider1HoleOvalLength = 2 * length;
float	slider1HoleOvalRadiusOut = 0.2;
float	slider1HoleOvalRadiusIn = 0.1;

float	rack2Cuboid1X = 2;
float	rack2Cuboid1Y = width;
float	rack2Cuboid1Z = width;

float	rack2Cuboid2X = width;
float	rack2Cuboid2Y = width;
float	rack2Cuboid2Z = 2 * width;

float	rack2HoleCuboidWidth = thickness;
float	rack2HoleCuboidSizeX = width + 2 * thickness;
float	rack2HoleCuboidSizeY = width + 2 * thickness;
float	rack2HoleCuboidSizeZ = width + 2 * thickness;

float	slider2CuboidSizeX = rack2Cuboid1X;
float	slider2CuboidSizeY = width;
float	slider2CuboidSizeZ = width;

float	slider2HoleOvalHeight = columnSizeX;
float	slider2HoleOvalLength = 2 * length;
float	slider2HoleOvalRadiusOut = 0.2;
float	slider2HoleOvalRadiusIn = 0.1;


float	crankRadius = width / 2;

float	crankOvalHeight = width;
float	crankOvalLength = length;
float	crankOvalRadius = crankRadius;

float	crankCylinder1Height = width + thickness;
float	crankCylinder1Radius = crankRadius;

float	crankCylinder2Height = 2 * width;
float	crankCylinder2Radius = crankRadius;


Mesh	base;
Mesh	column;
Mesh	rack1Cuboid;
Mesh	rack1HoleCuboid;
Mesh	rack2Cuboid1;
Mesh	rack2Cuboid2;
Mesh	rack2HoleCuboid;
Mesh	slider1Cuboid;
Mesh	slider1HoleOval;
Mesh	slider2Cuboid;
Mesh	slider2HoleOval;

Mesh	crankOval;
Mesh	crankCylinder1;
Mesh	crankCylinder2;

void myKeyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case '1':
		base.rotateY += baseRotateStep;
		if (base.rotateY > 360)
			base.rotateY -= 360;
		break;
	case '2':
		base.rotateY -= baseRotateStep;
		if (base.rotateY < 0)
			base.rotateY += 360;
		break;
	case '3':
		crankOval.rotateZ += baseRotateStep;
		if (crankOval.rotateZ == 360)
			crankOval.rotateZ = 0;

		crankCylinder2.rotateZ += baseRotateStep;
		if (crankOval.rotateZ == 360)
			crankOval.rotateZ = 0;

		break;
	case '4':
		crankOval.rotateZ -= baseRotateStep;
		if (crankOval.rotateZ < 0)
			crankOval.rotateZ += 360;

		crankCylinder2.rotateZ -= baseRotateStep;
		if (crankOval.rotateZ < 0)
			crankOval.rotateZ += 360;
		break;
	case 'r':
	case 'R':
		crankOval.rotateZ = 0;
		crankCylinder2.rotateZ = 0;
		base.rotateY = 0;
		break;
	case 'w':
	case 'W':
		bWireFrame = !bWireFrame;
		break;
	case '+':
		camera_dis += 1;
		break;
	case '-':
		camera_dis -= 1;
		break;
	default:
		cout << int(key) << endl;
	}

	glutPostRedisplay();
}
void keySpecial(int key, int x, int y) {
	switch (key)
	{
	case GLUT_KEY_UP:
		camera_height += 1;
		break;
	case GLUT_KEY_DOWN:
		camera_height -= 1;
		break;
	case GLUT_KEY_RIGHT:
		camera_angle -= 5;
		break;
	case GLUT_KEY_LEFT:
		camera_angle += 5;
		break;
	default:
		break;
	}
	
}
void drawAxis()
{
	glColor3f(0, 0, 1);
	glBegin(GL_LINES);
	glColor3f(1, 0, 0);
	glVertex3f(0, 0, 0);//x
	glVertex3f(4, 0, 0);

	glColor3f(0, 1, 0);
	glVertex3f(0, 0, 0);//y
	glVertex3f(0, 4, 0);

	glColor3f(0, 0, 1);
	glVertex3f(0, 0, 0);//z
	glVertex3f(0, 0, 4);
	glEnd();
}

void drawBase()
{
	glPushMatrix();

	base.slideY = baseHeight / 2;
	glTranslated(base.slideX, base.slideY, base.slideZ);
	glRotatef(base.rotateY, 0, 1, 0);

	if (bWireFrame)
		base.DrawWireframe();
	else
		base.DrawColor();

	glPopMatrix();
}
void drawColumn()
{
	glPushMatrix();

	column.slideY = columnSizeY / 2 + baseHeight;

	glTranslated(column.slideX, column.slideY, column.slideZ);
	glRotatef(base.rotateY, 0, 1, 0);

	if (bWireFrame)
		column.DrawWireframe();
	else
		column.DrawColor();

	glPopMatrix();
}
void drawRack1()
{
	// rack1Cuboid
	glPushMatrix();
	glRotatef(base.rotateY, 0, 1, 0);

	rack1Cuboid.slideY = height2;
	rack1Cuboid.slideZ = rack1CuboidSizeZ / 2 + columnSizeZ / 2;

	glTranslated(rack1Cuboid.slideX, rack1Cuboid.slideY, rack1Cuboid.slideZ);

	if (bWireFrame)
		rack1Cuboid.DrawWireframe();
	else
		rack1Cuboid.DrawColor();

	glPopMatrix();

	// rack1HoleCuboid
	glPushMatrix();
	glRotatef(base.rotateY, 0, 1, 0);

	rack1HoleCuboid.slideY = height2;
	rack1HoleCuboid.slideZ = rack1HoleCuboidSizeZ / 2.0 + columnSizeZ / 2.0 + rack1CuboidSizeZ;

	glTranslated(rack1HoleCuboid.slideX, rack1HoleCuboid.slideY, rack1HoleCuboid.slideZ);

	if (bWireFrame)
		rack1HoleCuboid.DrawWireframe();
	else
		rack1HoleCuboid.DrawColor();

	glPopMatrix();
}
void drawRack2(){
	//	Cuboid 1
	glPushMatrix();

	rack2Cuboid1.slideX = rack2Cuboid1X / 2 + columnSizeX / 2;
	rack2Cuboid1.slideY = height1;

	glRotatef(base.rotateY, 0, 1, 0);
	glTranslated(rack2Cuboid1.slideX, rack2Cuboid1.slideY, rack2Cuboid1.slideZ);

	if (bWireFrame)
		rack2Cuboid1.DrawWireframe();
	else
		rack2Cuboid1.DrawColor();

	glPopMatrix();

	// Cuboid 2
	glPushMatrix();

	rack2Cuboid2.slideX = rack2Cuboid1X * 0.65;
	rack2Cuboid2.slideY = height1;
	rack2Cuboid2.slideZ = rack2Cuboid2Z / 2 + width/2;

	glRotatef(base.rotateY, 0, 1, 0);
	glTranslated(rack2Cuboid2.slideX, rack2Cuboid2.slideY, rack2Cuboid2.slideZ);

	if (bWireFrame)
		rack2Cuboid2.DrawWireframe();
	else
		rack2Cuboid2.DrawColor();

	glPopMatrix();

	// Hole Cuboid
	glPushMatrix();

	rack2HoleCuboid.slideZ = rack2HoleCuboidSizeZ / 2 + rack2Cuboid1Z / 2 + rack2Cuboid2Z;
	rack2HoleCuboid.slideX = rack2Cuboid2.slideX;
	rack2HoleCuboid.slideY = height1;

	glRotatef(base.rotateY, 0, 1, 0);
	glTranslated(rack2HoleCuboid.slideX, rack2HoleCuboid.slideY, rack2HoleCuboid.slideZ);

	glRotatef(90, 0, 0, 1);
	if (bWireFrame)
		rack2HoleCuboid.DrawWireframe();
	else
		rack2HoleCuboid.DrawColor();

	glPopMatrix();
}
void drawSlider1(){
	// Slider1HoleOval
	glPushMatrix();

	glRotatef(base.rotateY, 0, 1, 0);
	
	slider1HoleOval.slideY = height1;
	slider1HoleOval.slideZ = slider1CuboidSizeZ / 2.0 + columnSizeZ / 2.0 + rack1CuboidSizeZ + rack1HoleCuboidWidth;
	{
		float angle = crankOval.rotateZ;
		//tjinlag
		if (angle < 90){
			angle = (int)crankOval.rotateZ % 90;
			slider1HoleOval.slideY -= 2 * length * sin(angle / 2 * DEG2RAD) * cos(angle / 2 * DEG2RAD);
		}
		else if (angle == 90){
			slider1HoleOval.slideY -= length;
		}
		else if (angle < 180){
			angle = 90 - (int)crankOval.rotateZ % 90;
			slider1HoleOval.slideY -= 2 * length * sin(angle / 2 * DEG2RAD) * cos(angle / 2 * DEG2RAD);
		}
		else if (angle < 270){
			angle = (int)crankOval.rotateZ % 90;
			slider1HoleOval.slideY += 2 * length * sin(angle / 2 * DEG2RAD) * cos(angle / 2 * DEG2RAD);
		}
		else if (angle == 270){
			slider1HoleOval.slideY += length;
		}
		else {
			angle = 90 - (int)crankOval.rotateZ % 90;
			slider1HoleOval.slideY += 2 * length * sin(angle / 2 * DEG2RAD) * cos(angle / 2 * DEG2RAD);
		}
		 
	}

	glTranslated(slider1HoleOval.slideX, slider1HoleOval.slideY, slider1HoleOval.slideZ);
	glRotatef(90, 1, 0, 0);

	if (bWireFrame)
		slider1HoleOval.DrawWireframe();
	else
		slider1HoleOval.DrawColor();

	glPopMatrix();

	// Slider1Cuboid
	glPushMatrix();

	glRotatef(base.rotateY, 0, 1, 0);

	slider1Cuboid.slideZ = slider1CuboidSizeZ / 2.0 + columnSizeZ / 2.0 + rack1CuboidSizeZ + rack1HoleCuboidWidth;
	slider1Cuboid.slideY = slider1CuboidSizeY / 2 + slider1HoleOval.slideY + slider1HoleOvalRadiusOut ;
	glTranslated(slider1Cuboid.slideX, slider1Cuboid.slideY, slider1Cuboid.slideZ);

	if (bWireFrame)
		slider1Cuboid.DrawWireframe();
	else
		slider1Cuboid.DrawColor();

	glPopMatrix();

}
void drawSlider2(){
	// Slider2HoleOval
	glPushMatrix();

	glRotatef(base.rotateY, 0, 1, 0);

	slider2HoleOval.slideX = 0;// -length;
	slider2HoleOval.slideY = height1;
	slider2HoleOval.slideZ = rack2HoleCuboid.slideZ;

	{
		float angle = crankOval.rotateZ;
		//tjinlag
		if (angle < 90){
			angle = 90 - (int)angle % 90;
			slider2HoleOval.slideX -= 2 * length * sin(angle / 2 * DEG2RAD) * cos(angle / 2 * DEG2RAD);
		}
		else if (angle == 90){
		}
		else if (angle < 180){
			angle = (int)angle % 90;
			slider2HoleOval.slideX += 2 * length * sin(angle / 2 * DEG2RAD) * cos(angle / 2 * DEG2RAD);
		}
		else if (angle < 270){
			angle = 90 - (int)angle % 90;
			slider2HoleOval.slideX += 2 * length * sin(angle / 2 * DEG2RAD) * cos(angle / 2 * DEG2RAD);
		}
		else if (angle == 270){
		}
		else {
			angle = (int)angle % 90;
			slider2HoleOval.slideX -= 2 * length * sin(angle / 2 * DEG2RAD) * cos(angle / 2 * DEG2RAD);
		}

	}


	glTranslated(slider2HoleOval.slideX, slider2HoleOval.slideY, slider2HoleOval.slideZ);

	glRotatef(90, 0, 0, 1);
	glRotatef(90, 1, 0, 0);

	if (bWireFrame)
		slider2HoleOval.DrawWireframe();
	else
		slider2HoleOval.DrawColor();

	glPopMatrix();

	// Slider2Cuboid
	glPushMatrix();

	glRotatef(base.rotateY, 0, 1, 0);

	slider2Cuboid.slideX = slider2CuboidSizeX / 2 + slider1HoleOvalRadiusOut + slider2HoleOval.slideX;
	slider2Cuboid.slideY = height1;
	slider2Cuboid.slideZ = slider2HoleOval.slideZ;

	glTranslated(slider2Cuboid.slideX, slider2Cuboid.slideY, slider2Cuboid.slideZ);

	if (bWireFrame)
		slider2Cuboid.DrawWireframe();
	else
		slider2Cuboid.DrawColor();

	glPopMatrix();
}
void drawCrank(){
	// Slider1Cuboid
	glPushMatrix();

	glRotatef(base.rotateY, 0, 1, 0);

	crankCylinder1.slideZ = crankCylinder1Height / 2 + width / 2;
	crankCylinder1.slideY = height1;

	glTranslated(crankCylinder1.slideX, crankCylinder1.slideY, crankCylinder1.slideZ);

	glRotatef(90, 1, 0, 0);

	if (bWireFrame)
		crankCylinder1.DrawWireframe();
	else
		crankCylinder1.DrawColor();

	glPopMatrix();

	// crankOval
	glPushMatrix();
	glRotatef(base.rotateY, 0, 1, 0);

	crankOval.slideX = - crankOvalLength / 2;
	crankOval.slideY = height1;
	crankOval.slideZ = crankOvalHeight / 2 + width / 2 + crankCylinder1Height;


	glTranslated(0, crankOval.slideY, 0);

	glRotatef(crankOval.rotateZ, 0, 0, 1);

	glTranslated(crankOval.slideX, 0, crankOval.slideZ);

	glRotatef(90, 1, 0, 0);


	if (bWireFrame)
		crankOval.DrawWireframe();
	else
		crankOval.DrawColor();

	glPopMatrix();

	// crankCylinder2
	glPushMatrix();

	glRotatef(base.rotateY, 0, 1, 0);

	crankCylinder2.slideX = -crankOvalLength;
	crankCylinder2.slideZ = crankOval.slideZ + width * 1.5;
	crankCylinder2.slideY = height1;

	glTranslatef(0, crankCylinder2.slideY, 0);
	
	glRotatef(crankCylinder2.rotateZ, 0, 0, 1);

	glTranslated(crankCylinder2.slideX, 0, crankCylinder2.slideZ);

	glRotatef(90, 1, 0, 0);

	if (bWireFrame)
		crankCylinder2.DrawWireframe();
	else
		crankCylinder2.DrawColor();

	glPopMatrix();
}

void myDisplay()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	{
		float eyeX, eyeY, eyeZ;
		eyeY = camera_height;
		eyeX = camera_dis * cos(camera_angle * DEG2RAD);
		eyeZ = camera_dis * sin(camera_angle * DEG2RAD);;
		gluLookAt(eyeX, eyeY, eyeZ, 0, 1, 0, 0, 1, 0);
	}
	//gluLookAt(6, 4, 6, 0, 1, 0, 0, 1, 0);
	

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, screenWidth, screenHeight);

	drawAxis();

	drawBase();
	drawColumn();
	drawRack1();
	drawSlider1();
	drawRack2();
	drawCrank();
	drawSlider2();


	glFlush();
	glutSwapBuffers();
}

void myInit()
{
	camera_angle = 45;
	camera_height = 3;
	camera_dis = 5;

	float	fHalfSize = 4;

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	glFrontFace(GL_CCW);
	glEnable(GL_DEPTH_TEST);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-fHalfSize, fHalfSize, -fHalfSize, fHalfSize, -1000, 1000);
}

int _tmain(int argc, _TCHAR* argv[])
{
	glutInit(&argc, (char**)argv); //initialize the tool kit
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);//set the display mode
	glutInitWindowSize(screenWidth, screenHeight); //set window size
	glutInitWindowPosition(200, 100); // set window position on screen
	glutCreateWindow("Simple Mechanical Device"); // open the screen window

	base.CreateCircle(baseHeight, baseRadius, baseRotateStep);
	base.SetColor(1);

	column.CreateCuboid(columnSizeX, columnSizeY, columnSizeZ);
	column.SetColor(2);

	rack1Cuboid.CreateCuboid(rack1CuboidSizeX, rack1CuboidSizeY, rack1CuboidSizeZ);
	rack1HoleCuboid.CreateHoleCuboid(rack1HoleCuboidSizeX, rack1HoleCuboidSizeY, rack1HoleCuboidSizeZ, rack1HoleCuboidWidth);
	rack1Cuboid.SetColor(3);
	rack1HoleCuboid.SetColor(3);

	slider1Cuboid.CreateCuboid(slider1CuboidSizeX, slider1CuboidSizeY, slider1CuboidSizeZ);
	slider1HoleOval.CreateHoleOval(slider1HoleOvalHeight, slider1HoleOvalLength, slider1HoleOvalRadiusOut, slider1HoleOvalRadiusIn);
	slider1Cuboid.SetColor(4);
	slider1HoleOval.SetColor(4);

	rack2Cuboid1.CreateCuboid(rack2Cuboid1X, rack2Cuboid1Y, rack2Cuboid1Z);
	rack2Cuboid2.CreateCuboid(rack2Cuboid2X, rack2Cuboid2Y, rack2Cuboid2Z);
	rack2HoleCuboid.CreateHoleCuboid(rack2HoleCuboidSizeX, rack2HoleCuboidSizeY, rack2HoleCuboidSizeZ, rack2HoleCuboidWidth);
	rack2Cuboid1.SetColor(5);
	rack2Cuboid2.SetColor(5);
	rack2HoleCuboid.SetColor(5);

	slider2Cuboid.CreateCuboid(slider2CuboidSizeX, slider2CuboidSizeY, slider2CuboidSizeZ);
	slider2HoleOval.CreateHoleOval(slider2HoleOvalHeight, slider2HoleOvalLength, slider2HoleOvalRadiusOut, slider2HoleOvalRadiusIn);
	slider2Cuboid.SetColor(6);
	slider2HoleOval.SetColor(6);
	
	crankOval.CreateOval(crankOvalHeight, crankOvalLength, crankOvalRadius);
	crankCylinder1.CreateCircle(crankCylinder1Height, crankCylinder1Radius);
	crankCylinder2.CreateCircle(crankCylinder2Height, crankCylinder1Radius);
	crankOval.SetColor(9);
	crankCylinder1.SetColor(9);
	crankCylinder2.SetColor(9);
	
	myInit();

	glutKeyboardFunc(myKeyboard);
	glutSpecialFunc(keySpecial);
	glutDisplayFunc(myDisplay);
	glutIdleFunc(myDisplay);

	glutMainLoop();
	return 0;
}
