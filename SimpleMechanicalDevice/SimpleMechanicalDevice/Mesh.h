#if !defined (_MESH_CLASS)
#define _MESH_CLASS

#include "supportClass.h"
#include <math.h>

class VertexID
{
public:
	int		vertIndex;
	int		colorIndex;
};

class Face
{
public:
	int			nVerts;
	VertexID*	vert;

	Face()
	{
		nVerts = 0;
		vert = NULL;
	}
	~Face()
	{
		if (vert != NULL)
		{
			delete[] vert;
			vert = NULL;
		}
		nVerts = 0;
	}
};

class Mesh
{
public:
	int		numVerts;
	Point3*	pt;

	int		numFaces;
	Face*	face;

	int r;

	float		slideX, slideY, slideZ;
	float		rotateX, rotateY, rotateZ;
	float		scaleX, scaleY, scaleZ;
public:
	Mesh()
	{
		numVerts = 0;
		pt = NULL;
		numFaces = 0;
		face = NULL;
		this->slideX = this->slideY = this->slideZ = 0.0;
		this->rotateX = this->rotateY = this->rotateZ = 0.0;
		this->scaleX = this->scaleY = this->scaleZ = 1.0;

		this->r = rand() % 10;
	}
	~Mesh()
	{
		if (pt != NULL)
		{
			delete[] pt;
		}
		/*if (face != NULL)
		{
		delete[] face;
		}*/
		numVerts = 0;
		numFaces = 0;
	}
	void DrawWireframe();
	void DrawColor();
	void SetColor(int colorIdx);

	void CreateTetrahedron();

	void CreateCube(float fSize = 1.0);
	void CreateHoleCube(float fSize = 2.0, float fWidth = 0.5);

	void CreateCuboid(float fSizeX = 1.0, float fSizeY = 1.0, float fSizeZ = 2.0);
	void CreateHoleCuboid(float fSizeX = 1.0, float fSizeY = 1.0, float fSizeZ = 2.0, float fWidth = 0.2);

	void CreateCircle(float fHeight = 2.0, float fRadius = 1.0, float fAngle = 5);
	void CreateHoleCircle(float fHeight = 1.0, float fRadiusOut = 2.0, float fRadiusIn = 1.6, float fAngle = 5);

	void CreateOval(float fHeight = 1.0, float fLength = 1.0, float fRadius = 1.0);
	void CreateHoleOval(float fHeight = 0.5, float fLength = 2.0, float fRadiusOut = 1.0, float fRadiusIn = -1);
	
};

#endif