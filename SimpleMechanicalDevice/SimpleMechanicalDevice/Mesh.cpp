#include "stdafx.h"
#include "Mesh.h"
#include <math.h>
#include <stdio.h>
using namespace std;

#define PI			3.1415926
#define	COLORNUM		14

#define DEG2RAD (PI/180.0f)


float	ColorArr[COLORNUM][3] = { { 1.0, 0.0, 0.0 }, { 0.0, 1.0, 0.0 }, { 0.0, 0.0, 1.0 },
{ 1.0, 1.0, 0.0 }, { 1.0, 0.0, 1.0 }, { 0.0, 1.0, 1.0 },
{ 0.3, 0.3, 0.3 }, { 0.5, 0.5, 0.5 }, { 0.9, 0.9, 0.9 },
{ 1.0, 0.5, 0.5 }, { 0.5, 1.0, 0.5 }, { 0.5, 0.5, 1.0 },
{ 0.0, 0.0, 0.0 }, { 1.0, 1.0, 1.0 } };

void Mesh::CreateCube(float	fSize)
{
	this->CreateCuboid(fSize, fSize, fSize);
}
void Mesh::CreateHoleCube(float fSize, float fWidth){
	this->CreateHoleCuboid(fSize, fSize, fSize, fWidth);
}
void Mesh::CreateTetrahedron()
{
	int i;
	numVerts = 4;
	pt = new Point3[numVerts];
	pt[0].set(0, 0, 0);
	pt[1].set(1, 0, 0);
	pt[2].set(0, 1, 0);
	pt[3].set(0, 0, 1);

	numFaces = 4;
	face = new Face[numFaces];

	face[0].nVerts = 3;
	face[0].vert = new VertexID[face[0].nVerts];
	face[0].vert[0].vertIndex = 1;
	face[0].vert[1].vertIndex = 2;
	face[0].vert[2].vertIndex = 3;
	for (i = 0; i<face[0].nVerts; i++)
		face[0].vert[i].colorIndex = 0;


	face[1].nVerts = 3;
	face[1].vert = new VertexID[face[1].nVerts];
	face[1].vert[0].vertIndex = 0;
	face[1].vert[1].vertIndex = 2;
	face[1].vert[2].vertIndex = 1;
	for (i = 0; i<face[1].nVerts; i++)
		face[1].vert[i].colorIndex = 1;


	face[2].nVerts = 3;
	face[2].vert = new VertexID[face[2].nVerts];
	face[2].vert[0].vertIndex = 0;
	face[2].vert[1].vertIndex = 3;
	face[2].vert[2].vertIndex = 2;
	for (i = 0; i<face[2].nVerts; i++)
		face[2].vert[i].colorIndex = 2;


	face[3].nVerts = 3;
	face[3].vert = new VertexID[face[3].nVerts];
	face[3].vert[0].vertIndex = 1;
	face[3].vert[1].vertIndex = 3;
	face[3].vert[2].vertIndex = 0;
	for (i = 0; i<face[3].nVerts; i++)
		face[3].vert[i].colorIndex = 3;
}
void Mesh::CreateCuboid(float fSizeX, float fSizeY, float fSizeZ){
	int i;

	float x = fSizeX / 2.0;
	float y = fSizeY / 2.0;
	float z = fSizeZ / 2.0;

	numVerts = 8;

	pt = new Point3[numVerts];
	pt[0].set(-x, y, z);
	pt[1].set(x, y, z);
	pt[2].set(x, y, -z);
	pt[3].set(-x, y, -z);
	pt[4].set(-x, -y, z);
	pt[5].set(x, -y, z);
	pt[6].set(x, -y, -z);
	pt[7].set(-x, -y, -z);


	numFaces = 6;
	face = new Face[numFaces];

	//Left face
	face[0].nVerts = 4;
	face[0].vert = new VertexID[face[0].nVerts];
	face[0].vert[0].vertIndex = 1;
	face[0].vert[1].vertIndex = 5;
	face[0].vert[2].vertIndex = 6;
	face[0].vert[3].vertIndex = 2;
	for (i = 0; i<face[0].nVerts; i++)
		face[0].vert[i].colorIndex = 0;

	//Right face
	face[1].nVerts = 4;
	face[1].vert = new VertexID[face[1].nVerts];
	face[1].vert[0].vertIndex = 0;
	face[1].vert[1].vertIndex = 3;
	face[1].vert[2].vertIndex = 7;
	face[1].vert[3].vertIndex = 4;
	for (i = 0; i<face[1].nVerts; i++)
		face[1].vert[i].colorIndex = 1;

	//top face
	face[2].nVerts = 4;
	face[2].vert = new VertexID[face[2].nVerts];
	face[2].vert[0].vertIndex = 0;
	face[2].vert[1].vertIndex = 1;
	face[2].vert[2].vertIndex = 2;
	face[2].vert[3].vertIndex = 3;
	for (i = 0; i<face[2].nVerts; i++)
		face[2].vert[i].colorIndex = 2;

	//bottom face
	face[3].nVerts = 4;
	face[3].vert = new VertexID[face[3].nVerts];
	face[3].vert[0].vertIndex = 7;
	face[3].vert[1].vertIndex = 6;
	face[3].vert[2].vertIndex = 5;
	face[3].vert[3].vertIndex = 4;
	for (i = 0; i<face[3].nVerts; i++)
		face[3].vert[i].colorIndex = 3;

	//near face
	face[4].nVerts = 4;
	face[4].vert = new VertexID[face[4].nVerts];
	face[4].vert[0].vertIndex = 4;
	face[4].vert[1].vertIndex = 5;
	face[4].vert[2].vertIndex = 1;
	face[4].vert[3].vertIndex = 0;
	for (i = 0; i<face[4].nVerts; i++)
		face[4].vert[i].colorIndex = 4;

	//Far face
	face[5].nVerts = 4;
	face[5].vert = new VertexID[face[5].nVerts];
	face[5].vert[0].vertIndex = 3;
	face[5].vert[1].vertIndex = 2;
	face[5].vert[2].vertIndex = 6;
	face[5].vert[3].vertIndex = 7;
	for (i = 0; i<face[5].nVerts; i++)
		face[5].vert[i].colorIndex = 5;
}
void Mesh::CreateHoleCuboid(float fSizeX, float fSizeY, float fSizeZ, float fWidth){
	// Check width pass
	if (2 * fWidth > fSizeX || 2 * fWidth > fSizeZ){
		this->CreateCuboid(fSizeX, fSizeY, fSizeZ);
		return;
	}

	float x = fSizeX / 2.0;
	float y = fSizeY / 2.0;
	float z = fSizeZ / 2.0;

	int idxVert = 0;
	int idxFace = 0;

	numVerts = 16;
	pt = new Point3[numVerts];

	// on the top
	pt[idxVert++].set(-x, y, z);
	pt[idxVert++].set(-x, y, -z);
	pt[idxVert++].set(x, y, -z);
	pt[idxVert++].set(x, y, z);

	pt[idxVert++].set(-x + fWidth, y, z - fWidth);
	pt[idxVert++].set(-x + fWidth, y, -z + fWidth);
	pt[idxVert++].set(x - fWidth, y, -z + fWidth);
	pt[idxVert++].set(x - fWidth, y, z - fWidth);

	//	bottom face
	pt[idxVert++].set(-x, -y, z);
	pt[idxVert++].set(-x, -y, -z);
	pt[idxVert++].set(x, -y, -z);
	pt[idxVert++].set(x, -y, z);

	pt[idxVert++].set(-x + fWidth, -y, z - fWidth);
	pt[idxVert++].set(-x + fWidth, -y, -z + fWidth);
	pt[idxVert++].set(x - fWidth, -y, -z + fWidth);
	pt[idxVert++].set(x - fWidth, -y, z - fWidth);


	numFaces = 16;
	face = new Face[numFaces];

	// out side:
	//	far: 1-0-8-9		right: 2-1-9-10			near: 3-2-10-11		left: 0-3-11-8
	for (int j = 0; j < 4; j++)
	{
		face[idxFace].nVerts = 4;
		face[idxFace].vert = new VertexID[4];

		face[idxFace].vert[0].vertIndex = (j + 1) % 4;
		face[idxFace].vert[1].vertIndex = j % 4;
		face[idxFace].vert[2].vertIndex = j % 4 + 8;
		face[idxFace].vert[3].vertIndex = (j + 1) % 4 + 8;

		for (int j = 0; j < 4; j++)
		{
			face[idxFace].vert[j].colorIndex = idxFace;
		}

		idxFace++;
	}

	//top face
	for (int i = 0; i < 4; i++)
	{
		face[i + 4].nVerts = 4;
		face[i + 4].vert = new VertexID[4];

		face[i + 4].vert[0].vertIndex = i % 4;
		face[i + 4].vert[1].vertIndex = (i + 1) % 4;
		face[i + 4].vert[2].vertIndex = (i + 1) % 4 + 4;
		face[i + 4].vert[3].vertIndex = (i % 4) + 4;

		for (int j = 0; j < 4; j++)
		{
			face[i + 4].vert[j].colorIndex = i + 2;
		}
	}

	//bottom face
	for (int i = 0; i < 4; i++)
	{
		face[i + 8].nVerts = 4;
		face[i + 8].vert = new VertexID[4];

		face[i + 8].vert[0].vertIndex = i % 4 + 8;
		face[i + 8].vert[1].vertIndex = (i + 1) % 4 + 8;
		face[i + 8].vert[2].vertIndex = (i + 1) % 4 + 12;
		face[i + 8].vert[3].vertIndex = i % 4 + 12;

		for (int j = 0; j < 4; j++)
		{
			face[i + 8].vert[j].colorIndex = i + 6;
		}
	}


	//far face (inside)
	face[12].nVerts = 4;
	face[12].vert = new VertexID[face[12].nVerts];
	face[12].vert[0].vertIndex = 4;
	face[12].vert[1].vertIndex = 5;
	face[12].vert[2].vertIndex = 13;
	face[12].vert[3].vertIndex = 12;
	for (int i = 0; i<face[12].nVerts; i++)
		face[12].vert[i].colorIndex = 12;
	//right face (inside)
	face[13].nVerts = 4;
	face[13].vert = new VertexID[face[13].nVerts];
	face[13].vert[0].vertIndex = 6;
	face[13].vert[1].vertIndex = 5;
	face[13].vert[2].vertIndex = 13;
	face[13].vert[3].vertIndex = 14;
	for (int i = 0; i<face[13].nVerts; i++)
		face[13].vert[i].colorIndex = 20;
	//near face (inside)
	face[14].nVerts = 4;
	face[14].vert = new VertexID[face[14].nVerts];
	face[14].vert[0].vertIndex = 6;
	face[14].vert[1].vertIndex = 7;
	face[14].vert[2].vertIndex = 15;
	face[14].vert[3].vertIndex = 14;
	for (int i = 0; i<face[14].nVerts; i++)
		face[14].vert[i].colorIndex = 14;
	//left face (inside)
	face[15].nVerts = 4;
	face[15].vert = new VertexID[face[15].nVerts];
	face[15].vert[0].vertIndex = 7;
	face[15].vert[1].vertIndex = 4;
	face[15].vert[2].vertIndex = 12;
	face[15].vert[3].vertIndex = 15;
	for (int i = 0; i<face[15].nVerts; i++)
		face[15].vert[i].colorIndex = 15;
}

void Mesh::CreateCircle(float fHeight, float fRadius, float fAngle){
	float angleNow = 0;

	numVerts = ceil(360 / fAngle) * 2;
	pt = new Point3[numVerts];
	for (int i = 0; i < numVerts / 2; i++)
	{
		pt[i].set(fRadius * cos(angleNow*DEG2RAD), fHeight / 2.0, fRadius * sin(angleNow*DEG2RAD));
		angleNow += fAngle;
	}

	angleNow = 0;
	for (int i = numVerts / 2; i < numVerts; i++)
	{
		pt[i].set(fRadius * cos(angleNow*DEG2RAD), -fHeight / 2.0, fRadius * sin(angleNow*DEG2RAD));
		angleNow += fAngle;
	}

	int numSideFaces = numVerts / 2;
	numFaces = 2 + numSideFaces;		// Top Face and Bottom Face

	face = new Face[numFaces];

	// numVerts -> numVerts/2 face side
	for (int i = 0; i < numSideFaces; i++)
	{
		face[i].nVerts = 4;
		face[i].vert = new VertexID[face[i].nVerts];

		face[i].vert[0].vertIndex = i;
		face[i].vert[1].vertIndex = i + 1;
		face[i].vert[2].vertIndex = i + numSideFaces + 1;
		face[i].vert[3].vertIndex = i + numSideFaces;

		for (int j = 0; j < 4; j++)
		{
			face[i].vert[j].colorIndex = i;
		}
		if (i == numSideFaces - 1){
			face[i].vert[1].vertIndex = 0;
			face[i].vert[2].vertIndex = numSideFaces;
		}
	}

	//// face top
	face[numSideFaces].nVerts = numVerts / 2;
	face[numSideFaces].vert = new VertexID[face[numSideFaces].nVerts];

	for (int i = 0; i < numVerts / 2; i++)
	{
		face[numSideFaces].vert[i].vertIndex = i;
		face[numSideFaces].vert[i].colorIndex = numSideFaces;
	}

	//// face bottom
	face[numSideFaces + 1].nVerts = numVerts / 2;
	face[numSideFaces + 1].vert = new VertexID[face[numVerts / 2 + 1].nVerts];

	for (int i = 0; i < numVerts / 2; i++)
	{
		face[numSideFaces + 1].vert[i].vertIndex = i + numSideFaces;
		face[numSideFaces + 1].vert[i].colorIndex = numSideFaces + 1;
	}
}
void Mesh::CreateHoleCircle(float fHeight, float fRadiusOut, float fRadiusIn, float fAngle){
	numVerts = ceil(360 / fAngle) * 2 * 2; // top bottom * 2

	pt = new Point3[numVerts];

	float angleNow = 0;
	// Add numVerts OutSide at Top:		0 -> numVerts/4
	for (int i = 0; i < numVerts / 4; i++)
	{
		pt[i].set(fRadiusOut * cos(angleNow*DEG2RAD), fHeight / 2.0, fRadiusOut * sin(angleNow*DEG2RAD));
		angleNow += fAngle;
	}

	angleNow = 0;
	// Add numVerts InSide at Top:		numVerts/4 -> numVerts/2
	for (int i = numVerts / 4; i < numVerts / 2; i++)
	{
		pt[i].set(fRadiusIn * cos(angleNow*DEG2RAD), fHeight / 2.0, fRadiusIn * sin(angleNow*DEG2RAD));
		angleNow += fAngle;
	}

	angleNow = 0;
	// Add numVerts OutSide at Bottom:		numVerts/2 -> 3*numVerts/4
	for (int i = numVerts / 2; i < 3 * numVerts / 4; i++)
	{
		pt[i].set(fRadiusOut * cos(angleNow*DEG2RAD), -fHeight / 2.0, fRadiusOut * sin(angleNow*DEG2RAD));
		angleNow += fAngle;
	}

	angleNow = 0;
	// Add numVerts Inside at Bottom:		numVerts/2 -> 3*numVerts/4
	for (int i = 3 * numVerts / 4; i < numVerts; i++)
	{
		pt[i].set(fRadiusIn * cos(angleNow*DEG2RAD), -fHeight / 2.0, fRadiusIn * sin(angleNow*DEG2RAD));
		angleNow += fAngle;
	}

	int numSideFaces = numVerts / 2 / 2;
	numFaces = 4 * numSideFaces;		// Top Face and Bottom Face, each have two

	face = new Face[numFaces];

	//			 OUTSIDE
	for (int i = 0; i < numSideFaces; i++)
	{
		face[i].nVerts = 4;
		face[i].vert = new VertexID[face[i].nVerts];

		face[i].vert[0].vertIndex = i;
		face[i].vert[1].vertIndex = i + 1;
		face[i].vert[2].vertIndex = 2 * numSideFaces + i + 1;
		face[i].vert[3].vertIndex = 2 * numSideFaces + i;

		if (i == numSideFaces - 1){
			face[i].vert[1].vertIndex = 0;
			face[i].vert[2].vertIndex = 2 * numSideFaces;
		}
	}

	//			 INSIDE
	for (int i = 0; i < numSideFaces; i++)
	{
		face[i + numSideFaces].nVerts = 4;
		face[i + numSideFaces].vert = new VertexID[4];

		face[i + numSideFaces].vert[0].vertIndex = numSideFaces + i;
		face[i + numSideFaces].vert[1].vertIndex = numSideFaces + i + 1;
		face[i + numSideFaces].vert[2].vertIndex = 3 * numSideFaces + i + 1;
		face[i + numSideFaces].vert[3].vertIndex = 3 * numSideFaces + i;

		if (i == numSideFaces - 1){
			face[i + numSideFaces].vert[1].vertIndex = numSideFaces;
			face[i + numSideFaces].vert[2].vertIndex = 3 * numSideFaces;
		}
	}

	// face top
	for (int i = 0; i < numSideFaces; i++)
	{
		face[i + 2 * numSideFaces].nVerts = 4;
		face[i + 2 * numSideFaces].vert = new VertexID[4];

		face[i + 2 * numSideFaces].vert[0].vertIndex = i;
		face[i + 2 * numSideFaces].vert[1].vertIndex = i + 1;
		face[i + 2 * numSideFaces].vert[2].vertIndex = numSideFaces + i + 1;
		face[i + 2 * numSideFaces].vert[3].vertIndex = numSideFaces + i;

		if (i == numSideFaces - 1){
			face[i + 2 * numSideFaces].vert[1].vertIndex = 0;
			face[i + 2 * numSideFaces].vert[2].vertIndex = numSideFaces;
		}
	}

	//// face bottom
	for (int i = 0; i < numSideFaces; i++)
	{
		face[i + 3 * numSideFaces].nVerts = 4;
		face[i + 3 * numSideFaces].vert = new VertexID[4];

		face[i + 3 * numSideFaces].vert[0].vertIndex = 2 * numSideFaces + i;
		face[i + 3 * numSideFaces].vert[1].vertIndex = 2 * numSideFaces + i + 1;
		face[i + 3 * numSideFaces].vert[2].vertIndex = 3 * numSideFaces + i + 1;
		face[i + 3 * numSideFaces].vert[3].vertIndex = 3 * numSideFaces + i;

		if (i == numSideFaces - 1){
			face[i + 3 * numSideFaces].vert[1].vertIndex = 2 * numSideFaces;
			face[i + 3 * numSideFaces].vert[2].vertIndex = 3 * numSideFaces;
		}
	}
}

void Mesh::CreateOval(float fHeight, float fLength, float fRadius){
	float angle = 90;
	float angleStep = 5;

	int numVertsEachSide = 180 / (int)angleStep + 1;

	numVerts = 4 * numVertsEachSide;

	pt = new Point3[numVerts];

	//		LEFT
	for (int i = 0; i < numVertsEachSide; i++)
	{
		pt[i].set(fRadius*cos(angle*DEG2RAD) - fLength / 2.0, fHeight / 2.0, fRadius*sin(angle*DEG2RAD));
		pt[i + 2 * numVertsEachSide].set(fRadius*cos(angle*DEG2RAD) - fLength / 2.0, -fHeight / 2.0, fRadius*sin(angle*DEG2RAD));

		angle += angleStep;
	}

	angle -= angleStep;
	//		RIGHT
	for (int i = 0; i < numVertsEachSide; i++)
	{
		pt[i + numVertsEachSide].set(fRadius*cos(angle*DEG2RAD) + fLength / 2.0, fHeight / 2.0, fRadius*sin(angle*DEG2RAD));
		pt[i + 3 * numVertsEachSide].set(fRadius*cos(angle*DEG2RAD) + fLength / 2.0, -fHeight / 2.0, fRadius*sin(angle*DEG2RAD));

		angle += angleStep;
	}

	numFaces = 2 * numVertsEachSide + 2;		// 2 = Top + Bot
	face = new Face[numFaces];

	// top
	face[2 * numVertsEachSide].nVerts = numVerts / 2;
	face[2 * numVertsEachSide].vert = new VertexID[numVerts / 2];
	for (int i = 0; i < 2 * numVertsEachSide; i++)
	{
		face[2 * numVertsEachSide].vert[i].vertIndex = i;
		face[2 * numVertsEachSide].vert[i].colorIndex = 0;
	}

	// bottom
	face[2 * numVertsEachSide + 1].nVerts = numVerts / 2;
	face[2 * numVertsEachSide + 1].vert = new VertexID[numVerts / 2];
	for (int i = 0; i < 2 * numVertsEachSide; i++)
	{
		face[2 * numVertsEachSide + 1].vert[i].vertIndex = 2 * numVertsEachSide + i;
		face[2 * numVertsEachSide + 1].vert[i].colorIndex = 1;
	}

	for (int i = 0; i < 2 * numVertsEachSide; i++)
	{
		face[i].nVerts = 4;
		face[i].vert = new VertexID[4];

		face[i].vert[0].vertIndex = i;
		face[i].vert[1].vertIndex = i + 1;
		face[i].vert[2].vertIndex = 2 * numVertsEachSide + i + 1;
		face[i].vert[3].vertIndex = 2 * numVertsEachSide + i;

		for (int j = 0; j < 4; j++)
		{
			face[i].vert[j].colorIndex = i;
		}

		if (i == 2 * numVertsEachSide - 1){
			face[i].vert[1].vertIndex = 0;
			face[i].vert[2].vertIndex = 2 * numVertsEachSide;
		}
	}
}
void Mesh::CreateHoleOval(float fHeight, float fLength, float fRadiusOut, float fRadiusIn){
	float angle = 90;
	float angleStep = 5;

	if (fRadiusIn < 0 || fRadiusIn >= fRadiusOut){
		fRadiusIn = 0.5 * fRadiusOut;
	}

	int numVertsEachSide = 180 / angleStep + 1;

	numVerts = 8 * numVertsEachSide;
	pt = new Point3[numVerts];

	//					OUTSIDE - OUTSIDE - OUTSIDE

	// 38 outside in the left:		=> -fLength/2.0
	//	0 -> 18: outside left top
	//	38 -> 56: outside left bottom
	for (int i = 0; i < numVertsEachSide; i++)
	{
		pt[i].set(fRadiusOut*cos(angle*DEG2RAD) - fLength / 2.0, fHeight / 2.0, fRadiusOut*sin(angle*DEG2RAD));
		pt[i + 2 * numVertsEachSide].set(fRadiusOut*cos(angle*DEG2RAD) - fLength / 2.0, -fHeight / 2.0, fRadiusOut*sin(angle*DEG2RAD));

		angle += angleStep;
	}
	// 38 outside in the right:		=> fLength/2.0
	//	19 -> 37:	outside right top
	//	57 -> 75:	outside right bottom
	angle -= angleStep;
	for (int i = 0; i < numVertsEachSide; i++)
	{
		pt[i + numVertsEachSide].set(fRadiusOut*cos(angle*DEG2RAD) + fLength / 2.0, fHeight / 2.0, fRadiusOut*sin(angle*DEG2RAD));
		pt[i + 3 * numVertsEachSide].set(fRadiusOut*cos(angle*DEG2RAD) + fLength / 2.0, -fHeight / 2.0, fRadiusOut*sin(angle*DEG2RAD));

		angle += angleStep;
	}


	//					INSIDE - INSIDE - INSIDE

	// 38 inside in the left:		=> -fLength/2.0
	//	76 -> 94: inside left top
	//	114 -> 132: inside left bottom
	angle = 90;
	for (int i = 0; i < numVertsEachSide; i++)
	{
		pt[i + 4 * numVertsEachSide].set(fRadiusIn*cos(angle*DEG2RAD) - fLength / 2.0, fHeight / 2.0, fRadiusIn*sin(angle*DEG2RAD));
		pt[i + 6 * numVertsEachSide].set(fRadiusIn*cos(angle*DEG2RAD) - fLength / 2.0, -fHeight / 2.0, fRadiusIn*sin(angle*DEG2RAD));

		angle += angleStep;
	}
	// 38 inside in the right:		=> fLength/2.0
	//	95 -> 113:	inside right top
	//	133 -> 151:	inside right bottom
	angle -= angleStep;
	for (int i = 0; i < numVertsEachSide; i++)
	{
		pt[i + 5 * numVertsEachSide].set(fRadiusIn*cos(angle*DEG2RAD) + fLength / 2.0, fHeight / 2.0, fRadiusIn*sin(angle*DEG2RAD));
		pt[i + 7 * numVertsEachSide].set(fRadiusIn*cos(angle*DEG2RAD) + fLength / 2.0, -fHeight / 2.0, fRadiusIn*sin(angle*DEG2RAD));

		angle += angleStep;
	}


	//			FACE	FACE	FACE	FACE	FACE	FACE
	numFaces = 8 * numVertsEachSide;
	face = new Face[numFaces];

	for (int i = 0; i < 2 * numVertsEachSide; i++)
	{
		face[i].nVerts = 4;
		face[i].vert = new VertexID[4];

		face[i].vert[0].vertIndex = i;
		face[i].vert[1].vertIndex = i + 1;
		face[i].vert[2].vertIndex = i + 2 * numVertsEachSide + 1;
		face[i].vert[3].vertIndex = i + 2 * numVertsEachSide;

		if (i == 2 * numVertsEachSide - 1){
			face[i].vert[1].vertIndex = 0;
			face[i].vert[2].vertIndex = 2 * numVertsEachSide;
		}
	}

	for (int i = 0; i < 2 * numVertsEachSide; i++)
	{
		face[i + 2 * numVertsEachSide].nVerts = 4;
		face[i + 2 * numVertsEachSide].vert = new VertexID[4];

		face[i + 2 * numVertsEachSide].vert[0].vertIndex = i + 4 * numVertsEachSide;
		face[i + 2 * numVertsEachSide].vert[1].vertIndex = i + 4 * numVertsEachSide + 1;
		face[i + 2 * numVertsEachSide].vert[2].vertIndex = i + 6 * numVertsEachSide + 1;
		face[i + 2 * numVertsEachSide].vert[3].vertIndex = i + 6 * numVertsEachSide;

		if (i == 2 * numVertsEachSide - 1){
			face[i + 2 * numVertsEachSide].vert[1].vertIndex = 4 * numVertsEachSide;
			face[i + 2 * numVertsEachSide].vert[2].vertIndex = 6 * numVertsEachSide;
		}
	}

	//	left top
	int idx = 4 * numVertsEachSide;
	for (int i = 0; i < numVertsEachSide - 1; i++)
	{
		face[idx].nVerts = 4;
		face[idx].vert = new VertexID[face[idx].nVerts];

		face[idx].vert[0].vertIndex = i;
		face[idx].vert[1].vertIndex = i + 1;
		face[idx].vert[2].vertIndex = i + 4 * numVertsEachSide + 1;
		face[idx].vert[3].vertIndex = i + 4 * numVertsEachSide;

		idx++;
	}
	//	right top
	for (int i = 0; i < numVertsEachSide - 1; i++)
	{
		face[idx].nVerts = 4;
		face[idx].vert = new VertexID[face[idx].nVerts];

		face[idx].vert[0].vertIndex = i + numVertsEachSide;
		face[idx].vert[1].vertIndex = i + numVertsEachSide + 1;
		face[idx].vert[2].vertIndex = i + 5 * numVertsEachSide + 1;
		face[idx].vert[3].vertIndex = i + 5 * numVertsEachSide;

		idx++;
	}

	face[idx].nVerts = 4;
	face[idx].vert = new VertexID[face[idx].nVerts];

	face[idx].vert[0].vertIndex = 0;
	face[idx].vert[1].vertIndex = 0 + 2 * numVertsEachSide - 1;
	face[idx].vert[2].vertIndex = 6 * numVertsEachSide - 1;
	face[idx].vert[3].vertIndex = 4 * numVertsEachSide;


	idx++;

	face[idx].nVerts = 4;
	face[idx].vert = new VertexID[face[idx].nVerts];

	face[idx].vert[0].vertIndex = numVertsEachSide - 1;
	face[idx].vert[1].vertIndex = numVertsEachSide;
	face[idx].vert[2].vertIndex = 5 * numVertsEachSide;
	face[idx].vert[3].vertIndex = 5 * numVertsEachSide - 1;


	idx++;


	//	left bottom
	for (int i = 0; i < numVertsEachSide - 1; i++)
	{
		face[idx].nVerts = 4;
		face[idx].vert = new VertexID[face[idx].nVerts];

		face[idx].vert[0].vertIndex = i + 2 * numVertsEachSide;
		face[idx].vert[1].vertIndex = i + 2 * numVertsEachSide + 1;
		face[idx].vert[2].vertIndex = i + 6 * numVertsEachSide + 1;
		face[idx].vert[3].vertIndex = i + 6 * numVertsEachSide;

		idx++;
	}

	//	right bottom
	for (int i = 0; i < numVertsEachSide - 1; i++)
	{
		face[idx].nVerts = 4;
		face[idx].vert = new VertexID[face[idx].nVerts];

		face[idx].vert[0].vertIndex = i + 3 * numVertsEachSide;
		face[idx].vert[1].vertIndex = i + 3 * numVertsEachSide + 1;
		face[idx].vert[2].vertIndex = i + 7 * numVertsEachSide + 1;
		face[idx].vert[3].vertIndex = i + 7 * numVertsEachSide;

		idx++;
	}

	face[idx].nVerts = 4;
	face[idx].vert = new VertexID[face[idx].nVerts];

	face[idx].vert[0].vertIndex = 2 * numVertsEachSide;
	face[idx].vert[1].vertIndex = 4 * numVertsEachSide - 1;
	face[idx].vert[2].vertIndex = 8 * numVertsEachSide - 1;
	face[idx].vert[3].vertIndex = 6 * numVertsEachSide;


	idx++;

	face[idx].nVerts = 4;
	face[idx].vert = new VertexID[face[idx].nVerts];

	face[idx].vert[0].vertIndex = 3 * numVertsEachSide - 1;
	face[idx].vert[1].vertIndex = 3 * numVertsEachSide;
	face[idx].vert[2].vertIndex = 7 * numVertsEachSide;
	face[idx].vert[3].vertIndex = 7 * numVertsEachSide - 1;

	idx++;
}

void Mesh::DrawWireframe()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	for (int f = 0; f < numFaces; f++)
	{
		glBegin(GL_POLYGON);
		for (int v = 0; v < face[f].nVerts; v++)
		{
			int		iv = face[f].vert[v].vertIndex;

			glVertex3f(pt[iv].x, pt[iv].y, pt[iv].z);
		}
		glEnd();
	}
}

void Mesh::DrawColor()
{
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	for (int f = 0; f < numFaces; f++)
	{
		glBegin(GL_POLYGON);
		for (int v = 0; v < face[f].nVerts; v++)
		{
			int		iv = face[f].vert[v].vertIndex;
			int		ic = face[f].vert[v].colorIndex;

			//random color
			//ic = (r + f) % COLORNUM;

			glColor3f(ColorArr[ic][0], ColorArr[ic][1], ColorArr[ic][2]);
			glVertex3f(pt[iv].x, pt[iv].y, pt[iv].z);
		}
		glEnd();
	}
}

void Mesh::SetColor(int colorIdx){
	for (int f = 0; f < numFaces; f++){
		for (int v = 0; v < face[f].nVerts; v++){
			face[f].vert[v].colorIndex = colorIdx;
		}
	}
}
